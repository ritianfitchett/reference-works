__author__ = 'Ian Fitchett'

#### imports ####
import csv
import random
########

#### globals ####
#NUM_CLUSTERS: the number of clusters that the program calculate
#FILE_NAME: the name of the csv file the program will read
NUM_CLUSTERS = 3
FILE_NAME = "QANTAS420_DATA__v043_temp_for_class.csv"
NUM_PROTOS = 30
########

#### buildArrays ####
# params:   files - string representation of the file name
# builds a list of vectors using the file. Function assumes that
# the first row of the csv file is titles
# returns:  an array of vectors that represent the data
def buildArrays(file):
    with open(file, 'r') as f:
        temp = [row for row in csv.reader(f.read().splitlines())]
        # get rid of the first row, it's just titles
        temp.pop(0)
        # create a 2D array that is the correct size
        Vectors = [[] for x in range(len(temp))]
        # populate the 2D array with vectors
        # for this list, subtract 1 because the last column is void
        for i in range(len(temp)):
            for j in range(len(temp[i])-1):
                Vectors[i].append(temp[i][j])
        return Vectors
########

#### calcEucliDist ####
#simple helper function that takes two vectors
#and calculates the euclideon distance between them.
#!! Assumes both lists are identical in size
def calcEucliDist(list1, list2):
    dist = 0
    for i in range(len(list1)):
        dist += pow(float(list1[i]) - float(list2[i]), 2)
    dist = pow(dist, 0.5)
    return dist
########

#### Kmeans ####
# params    vectors: list of vectors
#           prototypes: centers of clusters
def Kmeans(vectors, prototypes):
    #determine clusters
    tracker = [[] for x in range(len(prototypes))]
    new = [[0 for y in range(len(prototypes[0]))] for x in range(len(prototypes))]
    #print(prototypes)
    for i in vectors:
        min = 9999999
        for j in range(len(prototypes)):
            temp = calcEucliDist(i,prototypes[j])
            if temp < min:
                min = temp
                index = j
        tracker[index].append(i)
    #print(tracker)
    #calculate clusters
    for x in tracker: # x = a cluster
        if x:
            for i in range(len(x[0])): # i = index of some attribute of a vector
                avgVal = 0
                for j in x: # j = vector in cluster x
                    avgVal += float(j[i])
                avgVal = avgVal / len(x)
                new[tracker.index(x)][i] = avgVal
    #print(new)
    result = (tracker, new)
    return result
########

#### GenProtos ####
# params:   vectors: a list of all the vectors
#           numProtos: number of prototypes to return
#
# purpose:  Will find the min and max values of each vector
#           component, and will use that range to generate random
#           vectors
def GenProtos(vectors,numProtos):
    #instatiate variables
    result = []
    minVector = []
    maxVector = []
    minIndex = -1
    maxIndex = -1
    #Find min and max values of each component
    for i in range(len(vectors[0])):   # i = a single vector component
        min = 99999999
        max = -1
        for j in vectors:  # j = a single vector
            if j[i] > max:
                max = j[i]
                maxIndex = vectors.index(j)
            if j[i] < min:
                min = j[i]
                minIndex = vectors.index(j)
        minVector.append(vectors[minIndex][i])
        maxVector.append(vectors[maxIndex][i])
    # Generate random vectors
    for i in range(numProtos): # i is the prototype
        newVector = []
        for j in range(len(vectors[0])): # j is the component
            temp = random.uniform(float(minVector[j]),float(maxVector[j]))
            newVector.append(temp)
        result.append(newVector)
    return result
########

#### normalize ####
# This function uses domain knowledge to normalize vectors
# WARNING: THIS IS THE ONLY FUNCTION THAT IS EXCLUSIVE TO A SPECIFIC DATA SET
# THIS FUNCTION WILL NOT WORK AS INTENDED WITH ANY OTHER DATA SET
# Also this function changes the values in vectors, make sure not
# to call this function until all other processing is done first.
def normalize(vectors):
    for i in range(len(vectors)):
        for j in range(len(vectors[i])):
            vectors[i][j] = float(vectors[i][j])
            if j == 0 or j == 1:
                vectors[i][j] -= 0.0
                vectors[i][j] /= 5.0
            if j == 2:
                vectors[i][j] -= 2.0
                vectors[i][j] /= 8000.0
            if j == 3:
                vectors[i][j] -= 98.6
                vectors[i][j] /= 0.5
            if j == 4:
                vectors[i][j] -= 0.5
            if j == 5:
                vectors[i][j] -= 3.5
            if j == 6 or j == 7:
                vectors[i][j] -= 3.0
                vectors[i][j] /= 1000.0
            if j == 8:
                vectors[i][j] -= 0.5
                vectors[i][j] /= 1000.0
            if j == 9:
                vectors[i][j] -= 80.0
                vectors[i][j] /= 10.0
            if j == 10:
                vectors[i][j] -= 120.0
                vectors[i][j] /= 10.0
            if j == 11:
                vectors[i][j] -= 9000.0
                vectors[i][j] /= 2000.0
            if j == 12 or j == 13:
                vectors[i][j] -= 0.5
                vectors[i][j] /= 5.0
########

#### GenStandard9 ####
# Generates the standard 9 initial prototypes.
def GenStandard9():
    result = []
    result.append([7,8,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([42,3,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([1,1,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([2,9,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([13,2,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([18,9,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([33,2,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([38,9,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([51,10,0,0,0,0,0,0,0,0,0,0,0,0])
    return result
########

#### GenNormalized9 ####
# Generates the normalized 9 initial prototypes.
def GenNormalized9():
    result = []
    result.append([1.4,1.6,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([8.4,0.6,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([0.2,0.2,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([0.4,1.8,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([2.6,0.4,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([3.6,1.8,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([6.6,0.4,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([7.6,1.4,0,0,0,0,0,0,0,0,0,0,0,0])
    result.append([10.2,2,0,0,0,0,0,0,0,0,0,0,0,0])
    return result
########

#### truncate ####
# truncate function by David Z from StackOverflow
# This function is only used to format answers to
# be more readable when printing the table of results
def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])
########

#### condense ####
# This function is just used to condense the vector array to be more readable
def condense(vectors):
    for i in range(len(vectors)):
        for j in range(len(vectors[i])):
            vectors[i][j] = float(vectors[i][j])
            vectors[i][j] = truncate(vectors[i][j],3)
    return vectors
########

#### main ####
vectors= buildArrays(FILE_NAME)
# Calculate standard initial 9
prototypes = GenStandard9()
iterate = Kmeans(vectors,prototypes)
while prototypes != iterate[1]:
    prototypes = iterate[1]
    iterate = Kmeans(vectors,prototypes)
print("Calculate standard initial 9:")
print("Final prototypes are: " + str(condense(prototypes)))
#print("Which contains vectors:" + str(iterate[0]))
# Calculate standard initial 9 + 30 random
prototypes = GenStandard9()
protos = GenProtos(vectors,NUM_PROTOS)
for i in protos:
    prototypes.append(i)
iterate = Kmeans(vectors,prototypes)
while prototypes != iterate[1]:
    prototypes = iterate[1]
    iterate = Kmeans(vectors,prototypes)
print("Calculate standard initial 9 + random 30:")
print("Final prototypes are: " + str(condense(prototypes)))
#print("Which contains vectors:" + str(iterate[0]))
# Normalize Vectors
normalize(vectors)
# Calculate normalized initial 9
prototypes = GenNormalized9()
iterate = Kmeans(vectors,prototypes)
while prototypes != iterate[1]:
    prototypes = iterate[1]
    iterate = Kmeans(vectors,prototypes)
print("Calculate standard initial 9:")
print("Final prototypes are: " + str(condense(prototypes)))
#print("Which contains vectors:" + str(iterate[0]))
# Calculate normalized initial 9 + random 30
prototypes = GenStandard9()
protos = GenProtos(vectors,NUM_PROTOS)
for i in protos:
    prototypes.append(i)
iterate = Kmeans(vectors,prototypes)
while prototypes != iterate[1]:
    prototypes = iterate[1]
    iterate = Kmeans(vectors,prototypes)
print("Calculate standard normalized 9 + random 30:")
print("Final prototypes are: " + str(condense(prototypes)))
#print("Which contains vectors:" + str(iterate[0]))
########