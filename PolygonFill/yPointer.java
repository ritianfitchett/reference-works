/**
 * Created by Ian on 9/29/2015.
 * File:    yPointer.java
 * Purpose: To define a simple (int, bucket) tuple
 *          and give it some useful functionality.
 *          Used in edge table.
 *
 */
public class yPointer {
    private int y;
    private Bucket pointer;

    public void initYPointer(int index){
        this.y = index;
        this.pointer = null;
    }

    public void setPointer(Bucket pointer){
        this.pointer = pointer;
    }

    public int getY(){
        return this.y;
    }

    public Bucket next(){
        return this.pointer;
    }

    public boolean hasNext(){
        if(this.pointer == null) return false;
        return true;
    }

    public void killRow(){
        this.pointer = null;
    }
}
