/**
 * Created by Ian on 9/28/2015.
 * File:    Bucket.java
 * Purpose: To define a single bucket object.
 *          This is where all the functionality
 *          of storing and updating edges for
 *          scanlines is stored.
 *
 */
public class Bucket {
    public int ymax;
    public int x;
    public int dx;
    public int dy;
    public int sum;
    public Bucket next;

    //next value setter
    public void setNext(Bucket next){
        this.next = next;
    }

    //Gives a string representation of object
    public String toString(){
        String result = "";
        result += "[" + this.ymax + ", " + this.x + ", " + this. dx + ", " + this.dy + ", " + this.sum + "]";
        return result;
    }

    //Tests value equality of buckets
    public boolean equals(Bucket other){
        if(this.ymax == other.ymax &&
                this.x == other.x &&
                this.dx == other.dx &&
                this.dy == other.dy &&
                this.sum == other.sum)
            return true;
        else return false;
    }

    //Use two points to initialize the bucket. Assumes
    // (x0,y0) is left-most point.
    public void initBucket(int x0, int y0, int x1, int y1){
        this.ymax = Math.max(y0,y1);
        //pick the x value of the ymin
        if(this.ymax == y1) this.x = x0;
        else this.x = x1;
        this.dx = x1 - x0;
        //if a verticle line, go from bottom to top
        if(dx == 0) this.dy = Math.abs(y1-y0);
        else this.dy = y1 - y0;
        this.sum = 0;
        this.next = null;
    }

    //This function updates the x and sum values
    // for each y step.
    public void update(){
        this.x += (this.sum + this.dx)/this.dy;
        this.sum = (this.sum + this.dx)%this.dy;
    }
    

}
