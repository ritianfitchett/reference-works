__author__ = 'Ian Fitchett'

#### imports ####
import csv
########

#### globals ####
#NUM_CLUSTERS: the number of clusters that the program calculate
#FILE_NAME: the name of the csv file the program will read
NUM_CLUSTERS = 3
FILE_NAME = "HW_09_SHOPPING_CART_v037.csv"
########

#### buildArrays ####
# params:   files - string representation of the file name
# builds a list of vectors using the file. Function assumes that
# the first row of the csv file is titles and the first column
# is the ID numbers of each row.
# returns:  an array of vectors that represent the data
def buildArrays(file):
    with open(file, 'r') as f:
        temp = [row for row in csv.reader(f.read().splitlines())]
        # get rid of the first row, it's just titles
        temp.pop(0)
        # create a 2D array that is the correct size
        Vectors = [[] for x in range(len(temp))]
        # populate the 2D array with vectors
        for i in range(len(temp)):
            for j in range(len(temp[i])):
                if j != 0:
                    Vectors[i].append(temp[i][j])
        return Vectors
########

#### protoReduce ####
# Params:   prototypes: 2D array of vectors where
#                       each vector is a prototype
#           tracker:    2D array of ID's associated
#                       with each prototype
#
# purpose:  The bulk of the calculations done in this
#           program. It generate a complete table of
#           euclideon distances (ignores the diagonal)
#           and then picks the smallest distance and
#           merges those indices in both prototypes
#           and tracker and then returns them. Each
#           iteration of this function reduces the number
#           of prototypes by 1.
#
# returns:  tuple:      (prototypes,tracker)
def protoReduce(prototypes, tracker):
    # create a 2D array of the correct size
    distanceTable = [[0 for x in range(len(prototypes))] for x in range(len(prototypes))]
    EucliDist = 0
    for i in range(len(prototypes)):
        for j in range(len(prototypes)):
            EucliDist = calcEucliDist(prototypes[i],prototypes[j])
            distanceTable[i][j] = EucliDist
    min = 9999999999
    yindex = 0
    xindex = 0
    for i in distanceTable:
        for j in i:
            if float(j) < min and distanceTable.index(i) != distanceTable[distanceTable.index(i)].index(j):
                yindex = distanceTable.index(i)
                xindex = distanceTable[distanceTable.index(i)].index(j)
                min = float(j)
    new = [0 for x in range(len(prototypes[0]))]
    for i in range(len(new)):
        new[i] = (float(prototypes[yindex][i]) + float(prototypes[xindex][i]))/2.0
    # removing elements without interfering with the crucial ordering of the indeces
    # was a tricky thing to work around, had to assign temporary variables with
    # the values before removing them from the list or else bad things happen.
    y = prototypes[yindex]
    x = prototypes[xindex]
    ty = tracker[yindex]
    tx = tracker[xindex]
    if len(ty) > len(tx):
        print("smaller cluster = " + str(tx))
        print("larger cluster = " + str(ty))
    else:
        print("smaller cluster = " + str(ty))
        print("larger cluster = " + str(tx))
    newID = ty
    for i in tracker[xindex]:
        newID.append(i)
    print("new cluster = " + str(newID))
    tracker.remove(ty)
    tracker.remove(tx)
    tracker.append(newID)
    prototypes.remove(x)
    prototypes.remove(y)
    prototypes.append(new)
    return (prototypes, tracker)
########

#### calcEucliDist ####
#simple helper function that takes two vectors
#and calculates the euclideon distance between them.
#!! Assumes both lists are identical in size
def calcEucliDist(list1, list2):
    dist = 0
    for i in range(len(list1)):
        dist += pow(float(list1[i]) - float(list2[i]), 2)
    dist = pow(dist, 0.5)
    return dist
########

#### main ####
# Reads the csv fileFILE_NAME and clusters the data into NUM_CLUSTERS
# The program prints the center of mass of each cluster and the customer
# ID's that are associated with that cluster at each step of clustering.
# The tuple reduced holds the list of clusters in the first entry and
# the list of associated ID's in the second. While not labeled, they
# are aligned by index, therefore the members of reduced[0][n] are found
# in reduced[1][n]
print("Beginning clustering...")
print("Building arrays...")
vectors= buildArrays(FILE_NAME)
tracker = [[0] for x in range(len(vectors))]
for i in range(len(vectors)):
    tracker[i] = [i+1]
print("Calculating clusters...")
reduced = protoReduce(vectors, tracker)
count = 1
while len(reduced[0]) > NUM_CLUSTERS:
    print("step # " + str(count))
    reduced = protoReduce(reduced[0],reduced[1])
    for i in range(len(reduced[0])):
        print("cluster " + str(i) + " COM = " + str(reduced[0][i]))
        print("cluster " + str(i) + " consists of users: " + str(reduced[1][i]))
    count += 1
print("finished clustering output")
########

