#####################
Connect Four by Ian Fitchett
Last Updated: 3/3/2015
#####################
Description: networked game of Connect Four. The 
session manager can support multiple game sessions
of two players each. If one player quits, the session 
manager drops the entire session. Uses UDP.

Usage: 
First the server must be run on a computer. Once the
server has started, then the client programs may be run.
1) Download Files
2) Run the server from command line by navigating
   to the project folder and typing:
   $ java ConnectFourServer <host> <port>
3) For each user, run the client program from the
   command line by navigating to the project folder
   and typing:
   $ java ConnectFour <serverhost> <serverport> <clienthost> <clientport> <name>