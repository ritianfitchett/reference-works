# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is just a small list of some of the solo
projects I've done. Included are 3 implementations of major
data mining algorithms in python, with sample data included.
There is also a complete implementation of a 2D polygon fill
algorithm in java. The other project is a networked game of connect four in 
Java using a UDP connection. That project has its own 
readme included.

### How do I get set up? ###

For the python files, simply download them and run
the main.py file in each one. 
For Connect Four, follow the directions in its readme. 
For PolygonFill, first download all files. 
If using command line, compile the program using $ javac fillTest.java
run using $ java fillTest

### Questions/Feedback ###

email  ritianfitchett@gmail.com