__author__ = 'Ian Fitchett'

#### imports ####
import csv
import numpy as np

#### globals #####
Vectors = [[0 for x in range(8)] for x in range(300)]
MasterList = []
WingMaterial = []
Empennage = []
NoseWeight = []
PlaneWeight = []
WingCord = []
COM = []
MTBF = []
Secs = []
UVectors = [[0 for x in range(6)] for x in range(22)]
UMasterList = []
UWingMaterial = []
UEmpennage = []
UNoseWeight = []
UPlaneWeight = []
UWingCord = []
UCOM = []

#### buildArrays ####
# reads file of known vars and builds lists
def buildArrays(file):
    with open(file, 'r') as f:
        temp = [row for row in csv.reader(f.read().splitlines())]
        temp.pop(0)
        for i in temp:
            if(i[0] == "balsa"):
                WingMaterial.append(1.0)
            elif(i[0] == "film"):
                WingMaterial.append(2.0)
            elif(i[0] == "depron"):
                WingMaterial.append(3.0)
            else:
                WingMaterial.append(0.0)
            if(i[1] == "\tUsual"):
                Empennage.append(1.0)
            elif(i[1] == "\tV-Tail"):
                Empennage.append(2.0)
            elif(i[1] == "\tT-tail"):
                Empennage.append(3.0)
            else:
                Empennage.append(0.0)
            NoseWeight.append(float(i[2].lstrip("\t")))
            PlaneWeight.append(float(i[3].lstrip("\t")))
            WingCord.append(float(i[4].lstrip("\t")))
            COM.append(float(i[5].lstrip("\t")))
            MTBF.append(float(i[6].lstrip("\t")))
            Secs.append(float(i[7].lstrip("\t")))
        #Build MasterList
        MasterList.append(WingMaterial)
        MasterList.append(Empennage)
        MasterList.append(NoseWeight)
        MasterList.append(PlaneWeight)
        MasterList.append(WingCord)
        MasterList.append(COM)
        MasterList.append(MTBF)
        MasterList.append(Secs)

########

#### truncate ####
# truncate function by David Z from StackOverflow
# This function is only used to format answers to
# be more readable when printing the table of results
def truncate(f, n):
    '''Truncates/pads a float f to n decimal places without rounding'''
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])
########

#### buildUArrays ####
# reads file of unknown vars and builds lists
def buildUArrays(file):
    with open(file, 'r') as f:
        temp = [row for row in csv.reader(f.read().splitlines())]
        temp.pop(0)
        for i in temp:
            if(i[0] == "balsa"):
                UWingMaterial.append(1.0)
            elif(i[0] == "film"):
                UWingMaterial.append(2.0)
            elif(i[0] == "depron"):
                UWingMaterial.append(3.0)
            else:
                UWingMaterial.append(0.0)
            if(i[1] == "\tUsual"):
                UEmpennage.append(1.0)
            elif(i[1] == "\tV-Tail"):
                UEmpennage.append(2.0)
            elif(i[1] == "\tT-tail"):
                UEmpennage.append(3.0)
            else:
                UEmpennage.append(0.0)
            UNoseWeight.append(float(i[2].lstrip("\t")))
            UPlaneWeight.append(float(i[3].lstrip("\t")))
            UWingCord.append(float(i[4].lstrip("\t")))
            UCOM.append(float(i[5].lstrip("\t")))
        #build UMasterList
        UMasterList.append(UWingMaterial)
        UMasterList.append(UEmpennage)
        UMasterList.append(UNoseWeight)
        UMasterList.append(UPlaneWeight)
        UMasterList.append(UWingCord)
        UMasterList.append(UCOM)
########

#### main ####
#build the table using the file
buildArrays("HW08_KNOWN_DATA_for_kNN.csv")
#Formating and table printing, Make it readable
print("titles\t" + "WingMaterial\t" + "Empennage\t\t" + "NoseWeight\t\t" \
      + "PlaneWeight\t\t" + "WingCord\t\t" + "COM\t\t\t\t" + "MTBF\t\t\t" + "Secs\t")
for i in range (0,8):
    max = 0.0
    string = str(i)
    for j in range (0,8):
        #Using corrcoef from the numpy library to calculate Cross-Correlation Coefficient
        result = np.corrcoef(MasterList[i],MasterList[j])[0][1]
        string = string + "     \t" + str(truncate(result,5))
        if((abs(result)) > max and result < .99):
            max = abs(result)
    print(string)
print("Best correlation = " + str(max))
##Now we do K-NN, built the unknown lists
buildUArrays("HW08_UNKNOWN_DATA_for_kNN.csv")
#print(UMasterList)
#building Uvectors
for i in range(0,len(UMasterList)):
    for j in range(0,len(UMasterList[i])):
        #print(str(i) + " , " + str(j))
        UVectors[j][i] = (UMasterList[i][j])
#building vectors
for i in range(0,len(MasterList)):
    for j in range(0,len(MasterList[i])):
        #print(str(i) + " , " + str(j))
        Vectors[j][i] = (MasterList[i][j])
#print(UVectors)
#Now that we have vectors, for each Uvector
#We will find a nearest vector.
#print results
for i in UVectors:
    min = 100
    NeighborIndex = 0
    for j in Vectors:
        #print(str(UVectors.index(i)) + " , " + str(UVectors.index(j)))
        temp = abs(((np.sum(j) - j[6] - j[7]) - np.sum(i)))/22.0
        if(temp < min):
            NeighborIndex = Vectors.index(j)
            projMTBF = j[6]
            projSecs = j[7]
            min = temp
    print(str(UVectors.index(i)) + "'s nearest neighbor is " + str(NeighborIndex) + " with L1 distance of " + str(min)\
          + ". I'ts projected MTBF is " + str(projMTBF) + " and it's projected secs is " + str(projSecs))

